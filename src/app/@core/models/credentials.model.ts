export type CredentialTypes = 'social' | 'credentials';

export class Credentials {
    type: CredentialTypes;
    identity: string;
    password: string;
    remember_me: boolean;
}
