import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthApiService } from './api/auth-api.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Credentials } from '../models/credentials.model';
import { tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

const authStatusCookie = 'auth_status';

@Injectable()
export class AuthService extends AuthApiService {

    private _isAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    get isAuthenticated$(): Observable<boolean> {
        return this._isAuthenticated$.asObservable();
    }

    constructor(private cookies: CookieService, http: HttpClient) {
        super(http);
    }

    isAuthenticated(): boolean {
        return +this.cookies.get(authStatusCookie) === 1;
    }

    sync(): Observable<boolean> {
        this._isAuthenticated$.next(this.isAuthenticated());
        return this.isAuthenticated$;
    }

    login(credentials: Credentials): Observable<HttpResponse<void>> {
        return super.login(credentials).pipe(tap(() => this.sync()));
    }

    logout(): Observable<void> {
        return super.logout().pipe(tap(() => this.sync()));
    }
}
