import { Injectable } from '@angular/core';
import { HttpClient,  HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Credentials } from '../../models/credentials.model';


@Injectable()
export class AuthApiService {

    private tokenApi = `${environment.apiOrigin}/api/token`;
    private csrfApi = `${environment.apiOrigin}/api/csrf`;

    constructor(private http: HttpClient) { }

    protected login(credentials: Credentials): Observable<HttpResponse<void>> {
        return this.http.post<void>(this.tokenApi, credentials, {observe: 'response'});
    }

    protected logout(): Observable<void> {
        return this.http.delete<void>(this.tokenApi);
    }

    csrf(): Observable<void> {
        return this.http.get<void>(this.csrfApi);
    }
}
