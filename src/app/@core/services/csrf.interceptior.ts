import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AuthApiService } from './api/auth-api.service';
import { map, switchMap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

const csrfKeyCookie = 'XSRF-TOKEN';
const csrfHeader = 'X-XSRF-TOKEN';

@Injectable()
export class CsrfInterceptor implements HttpInterceptor {
    constructor(private cookieService: CookieService,
                private authApiService: AuthApiService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method !== 'GET') {
            return this.getCsrfToken().pipe(switchMap((token: string) => {
                return next.handle(request.clone({
                    setHeaders: {
                        [csrfHeader]: token,
                    },
                }));
            }));
        }

        return next.handle(request);
    }

    private getCsrfToken(): Observable<string> {
        const tokenFromCookies = this.cookieService.get(csrfKeyCookie);
        if (tokenFromCookies) {
            return of(tokenFromCookies);
        } else {
            return this.authApiService.csrf().pipe(map(() => this.cookieService.get(csrfKeyCookie)));
        }
    }
}
