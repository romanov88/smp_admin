import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { tap, map } from 'rxjs/operators';

@Injectable()
export class AnonymousGuard implements CanActivate {
    constructor(private authService: AuthService,
                private router: Router) {

    }
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
        return this.authService.isAuthenticated$
            .pipe(
                map(value => !value),
                tap(anonymous => {
                    if (!anonymous) {
                        return this.router.navigate(['/']);
                    }
                }),
            );
    }
}
