import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../@core/services/auth.service';

@Component({
    selector: 'admin-logout',
    template: '<div>Logging out, please wait...</div>',
})
export class LogoutComponent implements OnInit {

    redirectDelay: number = 1;

    constructor(protected authService: AuthService,
                protected router: Router) {
    }

    ngOnInit(): void {
        this.logout();
    }

    logout(): void {
        this.authService.logout().subscribe(() => {
            setTimeout(() => {
                return this.router.navigate(['/', 'auth', 'login']);
            }, this.redirectDelay);
        });
    }
}
