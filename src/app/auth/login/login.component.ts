import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Credentials } from '../../@core/models/credentials.model';
import { AuthService } from '../../@core/services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'admin-login',
    templateUrl: './login.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {

    redirectDelay: number = 1;

    errors: string[] = [];
    messages: string[] = [];
    credentials: Credentials = {
        type: 'credentials',
        identity: '',
        password: '',
        remember_me: false,
    };
    submitted: boolean = false;


    constructor(protected authService: AuthService,
                protected cd: ChangeDetectorRef,
                protected router: Router) {
    }

    login(): void {
        this.errors = [];
        this.messages = [];
        this.submitted = true;

        this.authService.login(this.credentials).pipe(
            finalize(() => {
                this.submitted = false;
                this.cd.detectChanges();
            }),
        ).subscribe(() => {
            this.messages.push('You\'ve successfully logged in');
            setTimeout(() => {
                return this.router.navigate(['/']);
            }, this.redirectDelay);
        },
        (response: HttpErrorResponse) => {
            if (response.error && response.error.message) {
                this.errors.push(response.error.message);
            }
        },
        );
    }

}