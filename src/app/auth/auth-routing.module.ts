import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthGuard } from '../@core/services/guards/auth.guard';
import { AnonymousGuard } from '../@core/services/guards/anonymous.guard';
import { LogoutComponent } from './logout/logout.component';
import { AuthComponent } from './auth.component';

export const routes: Routes = [{
    path: '',
    component: AuthComponent,
    children: [
        {
            path: '',
            component: LoginComponent,
            canActivate: [ AnonymousGuard ],
        },
        {
            path: 'login',
            component: LoginComponent,
            canActivate: [ AnonymousGuard ],
        },
        {
            path: 'logout',
            component: LogoutComponent,
            canActivate: [ AuthGuard ],
        },
    ],
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {
}
