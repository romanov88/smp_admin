import { Component } from '@angular/core';

@Component({
  selector: 'admin-maps',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MapsComponent {
}
