import { Component } from '@angular/core';

@Component({
  selector: 'admin-chartjs',
  styleUrls: ['./chartjs.component.scss'],
  templateUrl: './chartjs.component.html',
})
export class ChartjsComponent {}
