import { Component } from '@angular/core';

@Component({
  selector: 'admin-charts',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ChartsComponent {
}
