import { Component } from '@angular/core';

@Component({
  selector: 'admin-echarts',
  styleUrls: ['./echarts.component.scss'],
  templateUrl: './echarts.component.html',
})
export class EchartsComponent {}
