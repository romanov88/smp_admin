import { Component } from '@angular/core';

@Component({
  selector: 'admin-modal-overlays',
  template: `
    <router-outlet></router-outlet>
  `,
})

export class ModalOverlaysComponent {
}
