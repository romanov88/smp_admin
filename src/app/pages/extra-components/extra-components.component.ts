import { Component } from '@angular/core';

@Component({
  selector: 'admin-components',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class ExtraComponentsComponent {
}
