import { Component } from '@angular/core';

@Component({
  selector: 'admin-form-elements',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class FormsComponent {
}
