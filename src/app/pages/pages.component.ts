import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'admin-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <admin-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </admin-one-column-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;
}
