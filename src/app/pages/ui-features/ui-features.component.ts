import { Component } from '@angular/core';

@Component({
  selector: 'admin-ui-features',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class UiFeaturesComponent {
}
