import { Component } from '@angular/core';

@Component({
  selector: 'admin-miscellaneous',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class MiscellaneousComponent {
}
