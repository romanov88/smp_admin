import { Component } from '@angular/core';

@Component({
  selector: 'admin-tables',
  template: `<router-outlet></router-outlet>`,
})
export class TablesComponent {
}
