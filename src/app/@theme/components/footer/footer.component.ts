import { Component } from '@angular/core';

@Component({
  selector: 'admin-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      OneGlobus Admin 2020
    </span>
  `,
})
export class FooterComponent {
}
