import { Component, OnInit } from '@angular/core';
import { SeoService } from './@core/utils/seo.service';
import { AuthService } from './@core/services/auth.service';

@Component({
  selector: 'admin-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private seoService: SeoService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.sync();
    this.seoService.trackCanonicalChanges();
  }
}
